package com.lambdazen.bitsy;

import com.lambdazen.bitsy.ads.dict.Dictionary;
import com.lambdazen.bitsy.store.EdgeBean;
import com.lambdazen.bitsy.store.EdgeBeanJson;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.util.StringFactory;

public class BitsyEdge extends BitsyElement implements Edge, IEdge {
    String label;
    UUID outVertexId;
    UUID inVertexId;
    
    public BitsyEdge(UUID id, Dictionary properties, ITransaction tx, BitsyState state, int version, String label, UUID outVertexId, UUID inVertexId) {
        super(id, properties, tx, state, version);

        if (label == null) {
            throw new IllegalArgumentException("Edge label can not be null"); // Enforced by 2.3.0 test case
        }
        
        this.label = label;
        this.outVertexId = outVertexId;
        this.inVertexId = inVertexId;
    }

    public BitsyEdge(EdgeBean bean, ITransaction tx, BitsyState state) {
        this(bean.getId(), bean.getPropertiesDict(), 
                tx, state, bean.getVersion(), bean.getLabel(), bean.getOutVertexId(), bean.getInVertexId());
    }

    public EdgeBeanJson asJsonBean() {
        // The TX is usually not active at this point. So no checks.
        return new EdgeBeanJson((UUID)id, properties, version, label, outVertexId, inVertexId, state);
    }

    public String getLabel() {
        // There is no Tx validation for label because it is used even after deletion to update indexes, etc. 
        return label;
    }

    public Vertex getVertex(Direction dir) {
        tx.validateForQuery(this);
        
        Vertex ans = tx.getVertex(getVertexId(dir));
        
        // Vertex may disappear in READ_COMMITTED MODE
        if (ans == null) {
            throw new BitsyRetryException(BitsyErrorCodes.CONCURRENT_MODIFICATION, "The vertex in direction " + dir + " of the edge " + this + " was removed by another transaction");
        }

        return ans;
    }

    public UUID getInVertexId() {
        return inVertexId;
    }

    public UUID getOutVertexId() {
        return outVertexId;
    }

    public UUID getVertexId(Direction dir) {
        if (dir == Direction.IN) {
            return inVertexId;
        } else if (dir == Direction.OUT) {
            return outVertexId;
        } else {
            throw new IllegalArgumentException("Unsupported direction " + dir);
        }
    }

    public void incrementVersion() {
        this.version++;
    }
    
    public void remove() {
        tx.removeEdge(this);
    }

    public String toString() {
        return StringFactory.edgeString(this);
    }
}
