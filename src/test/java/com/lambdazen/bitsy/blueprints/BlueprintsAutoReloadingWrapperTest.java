package com.lambdazen.bitsy.blueprints;

import com.tinkerpop.blueprints.Graph;
import com.lambdazen.bitsy.BitsyGraph;
import com.lambdazen.bitsy.wrapper.BitsyAutoReloadingGraph;

public class BlueprintsAutoReloadingWrapperTest extends BlueprintsTest {
    @Override
    public Graph generateGraph() {
        Graph ans = super.generateGraph();
        
        return new BitsyAutoReloadingGraph((BitsyGraph)ans);
    }
}
