package com.lambdazen.bitsy.blueprints;

import com.tinkerpop.blueprints.Graph;
import com.lambdazen.bitsy.BitsyGraph;
import com.lambdazen.bitsy.BitsyIsolationLevel;

public class BlueprintsReadCommittedTest extends BlueprintsTest {
    @Override
    public Graph generateGraph() {
        Graph ans = super.generateGraph();
        
        ((BitsyGraph)ans).setTxIsolationLevel(BitsyIsolationLevel.READ_COMMITTED);
        
        return ans;
    }
}
