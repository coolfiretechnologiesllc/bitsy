package com.lambdazen.bitsy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.lambdazen.bitsy.store.FileBackedMemoryGraphStore;
import com.lambdazen.bitsy.util.CommittableFileLog;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Element;
import com.tinkerpop.blueprints.KeyIndexableGraph;
import com.tinkerpop.blueprints.ThreadedTransactionalGraph;
import com.tinkerpop.blueprints.TransactionalGraph;
import com.tinkerpop.blueprints.Vertex;
//import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;

public class BitsyGraphTest extends FileBasedTestCase {
    TransactionalGraph graph;
    Path dbPath;
    Random rand = new Random();
    private Throwable toThrow;
    
    public boolean isPersistent() {
        return true;
    }
    
    public void setUp() throws IOException {
        CommittableFileLog.setLockMode(false);
        
        setUp(true);
    }
    
    public void setUp(boolean delete) throws IOException {
        System.out.println("Setting up graph");
        
        this.dbPath = tempDir("temp-bitsy-graph", delete);
        graph = new BitsyGraph(dbPath);

//        this.dbPath = tempDir("temp-neo4j-graph", delete);
//        graph = new Neo4jGraph(dbPath.toString());
    }

    protected static void deleteDirectory(final File directory) {
        deleteDirectory(directory, true);
    }
    
    protected static void deleteDirectory(final File directory, boolean deleteDir) {
        if (directory.exists()) {
            for (File file : directory.listFiles()) {
                if (file.isDirectory()) {
                    deleteDirectory(file);
                } else {
                    file.delete();
                }
            }
            
            if (deleteDir) {
                directory.delete();
            }
        }
    }
    
    public void tearDown() {
        System.out.println("Tearing down graph");
        graph.shutdown();
    }

    public void testLoop() throws IOException {
        int numRuns = 3;
        for (int run = 0; run < numRuns; run++) {
            //System.out.println("Run #" + run);

            int numPerCommit = 10;
            int numCommit = 10;
            int numVertices = numPerCommit * numCommit;
            Object[] vids = new Object[numVertices];

            long ts = System.currentTimeMillis();
            for (int i = 0; i < numVertices; i++) {
                Vertex v = graph.addVertex(null);
                v.setProperty("rand", rand.nextInt());
                v.setProperty("count", i);
                vids[i] = v.getId();

                if (i % numPerCommit == 0) {
                    graph.commit();
                }

                // Make sure the vertex is there in the middle of the Tx
                assertEquals(new Integer(i), graph.getVertex(vids[i]).getProperty("count"));
            }
            graph.commit();

            double duration = System.currentTimeMillis() - ts;
            System.out.println("Took " + duration + "ms to insert " + numVertices + " vertices. Rate = " + (duration / numVertices) + "ms per vertex");

            // Make sure vertices are there
            for (int i = 0; i < numVertices; i++) {
                Vertex v = graph.getVertex(vids[i]);
                assertEquals(i, v.getProperty("count"));
            }

            // if (rand.nextDouble() < 2) {
            // return;
            // }

            // Stop and start
            if (isPersistent()) {
                tearDown();
                setUp(false);
            }

            // Make sure vertices are still there
            for (int i = 0; i < numVertices; i++) {
                Vertex v = graph.getVertex(vids[i]);
                assertNotNull(v);
                assertEquals(i, v.getProperty("count"));
            }

            // Now add edges
            Object[] eids = new Object[numVertices];
            for (int i = 0; i < numVertices; i++) {
                Vertex vOut = graph.getVertex(vids[i]);
                Vertex vIn = graph.getVertex(vids[(i + 1) % numVertices]);
                Edge e = graph.addEdge(null, vOut, vIn, "foo");
                e.setProperty("rand", rand.nextInt());
                e.setProperty("count", i);

                eids[i] = e.getId();

                if (i % numPerCommit == 0) {
                    graph.commit();
                }

                // Make sure the edge is there in the middle of the Tx
                // Using toString() to get the edge
                Edge qEdge = graph.getEdge(eids[i].toString());
                assertEquals(new Integer(i), qEdge.getProperty("count"));
                assertEquals(vOut, qEdge.getVertex(Direction.OUT));
                assertEquals(vIn, qEdge.getVertex(Direction.IN));
            }
            graph.commit();

            // Make sure that the edges are there
            for (int i = 0; i < numVertices; i++) {
                Edge e = graph.getEdge(eids[i]);
                assertEquals(i, e.getProperty("count"));
            }

            // Now modify the edges. Delete even ones
            for (int i = 0; i < numVertices; i++) {
                Edge e = graph.getEdge(eids[i]);
                e.setProperty("count", i + 1);

                eids[i] = e.getId();

                if (i % numPerCommit == 0) {
                    graph.commit();
                    
                    try {
                        e.getProperty("foo");
                    } catch (BitsyException ex) {
                        assertEquals(BitsyErrorCodes.ACCESS_OUTSIDE_TX_SCOPE, ex.getErrorCode());
                    }
                }

                // Make sure the edge is there in the middle of the Tx
                Edge qEdge = graph.getEdge(eids[i]);
                assertEquals(new Integer(i + 1), qEdge.getProperty("count"));
                
                // Traverse to the vertices
                Vertex vOut = qEdge.getVertex(Direction.OUT);
                assertEquals(new Integer(i), vOut.getProperty("count"));
                
                Vertex vIn = qEdge.getVertex(Direction.IN);
                assertEquals(new Integer((i + 1) % numVertices), vIn.getProperty("count"));
                
                // Run queries in the middle of the Tx
                assertEquals(qEdge, vIn.getEdges(Direction.IN).iterator().next());
                assertEquals(qEdge, vOut.getEdges(Direction.OUT).iterator().next());
                
                if (i % 2 == 0) {
                    graph.removeEdge(qEdge);

                    // Run queries in the middle of the Tx
                    if (rand.nextBoolean()) {
                        assertFalse(vIn.getEdges(Direction.IN).iterator().hasNext());
                        assertFalse(vOut.getEdges(Direction.OUT).iterator().hasNext());
                    } else {
                        try {
                            vIn.getEdges(Direction.IN).iterator().next();
                            fail("Expecting no element");
                        } catch (NoSuchElementException e1) {
                        }
                        
                        try {
                            vOut.getEdges(Direction.OUT).iterator().next();
                            fail("Expecting no element");
                        } catch (NoSuchElementException e2) {
                        }
                    }
                    
                    try { 
                        vIn.getEdges(Direction.IN).iterator().next();
                        fail("Can't access a missing edge");
                    } catch (NoSuchElementException ex) {
                        // Good
                    }
                    
                    try {
                        qEdge.getProperty("count");
                        fail("Can not access deleted edges");
                    } catch (BitsyException ex) {
                        assertEquals(BitsyErrorCodes.ELEMENT_ALREADY_DELETED, ex.getErrorCode());
                    }
                }
            }
            graph.commit();

            // Stop and start
            if (isPersistent()) {
                tearDown();
                //assertTrue(rand.nextDouble() > 5);
                setUp(false);
            }

            // Make sure vertices are still there
            for (int i = 0; i < numVertices; i++) {
                Vertex v = graph.getVertex(vids[i]);
                // System.out.println("Looking for " + vids[i]);
                
                assertEquals(i, v.getProperty("count"));
            }

            // Make sure that the edges are there
            for (int i = 0; i < numVertices; i++) {
                Edge e = graph.getEdge(eids[i]);
                
                if (i % 2 == 0) {
                    assertNull(e);
                } else {
                    // i was changed to i+1
                    assertNotNull("Could not find edge for UUID " + eids[i], e);
                    assertEquals(i + 1, e.getProperty("count"));
                }
            }
            
            // Stop and start
            if (isPersistent()) {
                tearDown();
                setUp(false);
            }
            
            // Make sure vertices are still there. Modify odd ones and delete even ones 
            for (int i = 0; i < numVertices; i++) {
                Vertex v = graph.getVertex(vids[i]);
                assertEquals(i, v.getProperty("count"));
                
                if (i % 2 == 0) {
                    graph.removeVertex(v);
                } else {
                    v.setProperty("count", i + 1);
                }

                if (i % numPerCommit == 0) {
                    graph.commit();
                    
                    try {
                        v.getProperty("count");
                    } catch (BitsyException ex) {
                        assertEquals(BitsyErrorCodes.ACCESS_OUTSIDE_TX_SCOPE, ex.getErrorCode());
                    }
                }
                
                // Check the vertex in the middle of the Tx
                Vertex qv = graph.getVertex(vids[i]);
                if (i % 2 == 0) {
                    assertNull(qv);
                } else {
                    assertEquals(i + 1, v.getProperty("count"));
                }
            }
            graph.commit();
            
            // Make sure that only odd vertices are still there 
            for (int i = 0; i < numVertices; i++) {
                Vertex v = graph.getVertex(vids[i]);
                
                if (i % 2 == 0) {
                    assertNull(v);
                } else {
                    assertEquals(i + 1, v.getProperty("count"));
                }
            }
            
            // Make sure that the edges are gone (even vertices are deleted)
            for (int i = 0; i < numVertices; i++) {
                Edge e = graph.getEdge(eids[i]);
                assertNull(e);
            }
            
            // Stop and start
            if (isPersistent()) {
                tearDown();
                setUp(false);
            }
            
            // Make sure that the edges are gone (even vertices are deleted)
            for (int i = 0; i < numVertices; i++) {
                Edge e = graph.getEdge(eids[i]);
                assertNull(e);
            }
            
            // Make sure that only odd vertices are still there 
            for (int i = 0; i < numVertices; i++) {
                Vertex v = graph.getVertex(vids[i]);
                
                if (i % 2 == 0) {
                    assertNull(v);
                } else {
                    assertEquals(i + 1, v.getProperty("count"));
                }
            }
        }
    }
    
    public void testClique() throws IOException {
        // Tests indexes in a clique graph
        int numPerCommit = 10;
        int numVertices = 29;
        int numRuns = 3;
        
        KeyIndexableGraph kig = (KeyIndexableGraph)graph;
        if (!kig.getIndexedKeys(Vertex.class).contains("vmod3")) {
            kig.createKeyIndex("vmod3", Vertex.class);
        } else {
            kig.dropKeyIndex("vmod3", Vertex.class);
            kig.createKeyIndex("vmod3", Vertex.class);
        }
        
        if (!kig.getIndexedKeys(Edge.class).contains("emod3")) {
            kig.createKeyIndex("emod3", Edge.class);
        } else {
            kig.dropKeyIndex("emod3", Edge.class);
            kig.createKeyIndex("emod3", Edge.class);
        }
        
        int txc = 0;
        Object[] vids = new Object[numVertices];
        Object[][] eids = new Object[numVertices][numVertices];
        long startLong = rand.nextLong();
        for (int run = 0; run < numRuns; run++) {
            if ((run == numRuns - 1) && ((BitsyGraph)graph).getStore().allowFullGraphScans()) {
                // Last run without index
                kig.dropKeyIndex("vmod3", Vertex.class);
                kig.dropKeyIndex("emod3", Edge.class);
            }
            
            // Change the starting long to look for index replacement logic
            long oldStartLong = startLong;
            startLong = rand.nextLong();
            int[] edgeCount = new int[] {0, 0, 0}; 
            
            for (int i=0; i < numVertices; i++) {
                if (i == numVertices / 2) {
                    if (isPersistent()) {
                        tearDown();
                        
                        setUp(false);
                    }
                }

                Vertex v;
                if  (run == 0) {
                    v = graph.addVertex(null);
                    vids[i] = v.getId();
                } else {
                    v = graph.getVertex(vids[i]);
                    assertEquals(vids[i], v.getId());
                }

                v.setProperty("vmod3", startLong + i % 3);
                
                // Make sure that the index works on committed and uncommitted data
                if ((txc++ % numPerCommit == 0) && rand.nextDouble() < 2) {
                    graph.commit();
                    
                    // Add a random vertex without this property
                    Vertex dummyV = graph.addVertex(null);
                    if (i % 2 == 0) dummyV.setProperty("notvmod3", 1);
                }
                
//                System.out.println("Testing vertex " + i + ". Run #" + run);
                
                // Negative test on index value in previous iter
                if (i > numVertices - 3) {
                    if (rand.nextBoolean()) {
                        assertFalse(graph.getVertices("vmod3", oldStartLong + i % 3).iterator().hasNext());
                    } else {
                        try {
                            graph.getVertices("vmod3", oldStartLong + i % 3).iterator().next();
                            fail("Expecting no element");
                        } catch (NoSuchElementException e1) {
                        }
                    }
                }
                
                Iterator<Vertex> viter = graph.getVertices("vmod3", startLong +  i % 3).iterator();
                int count = 0;
                while (viter.hasNext()) {
                    Vertex qv = viter.next();

                    boolean matchesSomething = false;
                    for (int div3 = i % 3; div3 <= i; div3 += 3) {
                        if (vids[div3].equals(qv.getId())) {
                            matchesSomething = true;
                            //System.out.println("Found match for " + div3);
                        }
                        matchesSomething = true;
                    }
                    assertTrue(matchesSomething);

                    count++;
                    //System.out.println("Count = " + count);
                }
                
                assertEquals(1 + i / 3, count);
                
                // Now add edges
                for (int j=0; j < i; j++) {
                    Edge e;
                    if (run == 0) {
                        Vertex outV = graph.getVertex(vids[j]);
                        Vertex inV =  graph.getVertex(vids[i]);

                        e = graph.addEdge(null, outV, inV, "test");
                        eids[i][j] = e.getId();
                    } else {
                        e = graph.getEdge(eids[i][j]);
                    }
                    
                    e.setProperty("emod3", "E" + (startLong + ((i-j) % 3)));
                    Object eid = e.getId();

                    // Make sure that the index works on committed and uncommitted data
                    if (txc++ % numPerCommit == 0) {
                        graph.commit();
                    }
                    
                    // Negative test on index value in previous iter
                    if ((i == numVertices - 1) && (j >= numVertices - 3)) {
                        assertFalse(graph.getEdges("emod3", "E" + (oldStartLong + ((i-j) % 3))).iterator().hasNext());
                    }

                    //System.out.println("Testing edge from " + j + " to " + i);
                    Iterator<Edge> eiter = graph.getEdges("emod3", "E" + (startLong + ((i-j) % 3))).iterator();
                    edgeCount[(i-j) % 3]++;
                    
                    int ecount = 0;
                    boolean matchesThis = false;
                    while (eiter.hasNext()) {
                        Edge qe = eiter.next();

                        if (eid.equals(qe.getId())) {
                            matchesThis = true;
                            //System.out.println("Found edge match");
                        }
                        
                        ecount++;
                    }
                    
                    assertTrue(matchesThis);
                    assertEquals(edgeCount[(i-j) % 3], ecount);
                }
                
                graph.commit();
            }
        }
        
        // Remove vertex props
        for (int i=0; i < numVertices; i++) {
            Vertex v = graph.getVertex(vids[i]);
            v.removeProperty("vmod3");
            
            Iterator<Edge> vOut = v.getEdges(Direction.OUT).iterator();
            while (vOut.hasNext()) {
                vOut.next().removeProperty("emod3");
            }
            
            // Add a random vertex without this property
            Vertex dummyV = graph.addVertex(null);
            dummyV.setProperty("notvmod3", 1);
            Object dummyVId = dummyV.getId();
            
            // Commit & remove
            graph.commit();
            dummyV = graph.getVertex(dummyVId);
            graph.removeVertex(dummyV);
        }

        // Make sure the index is empty
        for (int i=0; i < 3; i++) {
            assertFalse(graph.getVertices("vmod3", startLong + i % 3).iterator().hasNext());
        }

        // Add it back
        for (int i=0; i < numVertices; i++) {
            Vertex v = graph.getVertex(vids[i]);
            assertNull(v.getProperty("vmod3"));
            v.setProperty("vmod3", startLong);
        }

        // Make sure the index is non-empty before commit
        assertTrue(graph.getVertices("vmod3", startLong).iterator().hasNext());
        
        graph.commit();

        // ... and after
        for (int i=0; i < 3; i++) {
            assertTrue(graph.getVertices("vmod3", startLong).iterator().hasNext());
        }
        
        // Remove it again
        for (int i=0; i < numVertices; i++) {
            Vertex v = graph.getVertex(vids[i]);
            v.removeProperty("vmod3");
        }
        graph.commit();
        
        for (int i=0; i < 3; i++) {
            assertFalse(graph.getVertices("vmod3", startLong + i % 3).iterator().hasNext());
        }
        graph.commit();
        
        // Remove edge props
        for (int i=0; i < numVertices; i++) {
            for (int j=0; j < i; j++) {
                if (eids[i][j] == null) {
                    continue;
                }
                
                Edge e = graph.getEdge(eids[i][j]);
                e.removeProperty("emod3");
            }
            
            // Add a random edge without this property
            Vertex dummyV1 = graph.addVertex(null);
            dummyV1.setProperty("notvmod3", 1);
            
            Vertex dummyV2 = graph.addVertex(null);
            dummyV2.setProperty("notvmod3", 1);
            
            Edge e =graph.addEdge(null, dummyV1, dummyV2, "test");
            e.setProperty("notemod3", "E123");
        }

        for (int i=0; i < numVertices; i++) {
            for (int j=0; j < i; j++) {
                if (eids[i][j] == null) {
                    continue;
                }
                assertFalse(graph.getEdges("emod3", "E" + (startLong + ((i-j) % 3))).iterator().hasNext());
            }
        }
        
        graph.commit();
        
        for (int i=0; i < numVertices; i++) {
            for (int j=0; j < i; j++) {
                if (eids[i][j] == null) {
                    continue;
                }
                assertFalse(graph.getEdges("emod3", "E" + (startLong + ((i-j) % 3))).iterator().hasNext());
            }
        }

        // Remove edges
        for (int i=0; i < numVertices; i++) {
            for (int j=0; j < i; j++) {
                if ((i + j) % 10 == 0) {
                    graph.getEdge(eids[i][j]).remove();
                }
            }
        }
        graph.commit();
        
        // Remove vertices
        for (int i=0; i < numVertices; i++) {
            graph.getVertex(vids[i]).remove();
        }

        graph.commit();
    }
    
    public void testTypes() throws IOException {
        int numPerCommit = 10;
        int numCommit = 10;
        int numVertices = numPerCommit * numCommit;
        Object[] vids = new Object[numVertices];

        long ts = System.currentTimeMillis();
        for (int i = 0; i < numVertices; i++) {
            Vertex v = graph.addVertex(null);
            v.setProperty("int", i);
            v.setProperty("long", (long) i);
            v.setProperty("Integer", new Integer(i));
            v.setProperty("Long", new Long(i));
            v.setProperty("double", (double) i + 0.1);
            v.setProperty("Double", new Double(i + 0.1));
            v.setProperty("float", (float) i + 0.1);
            v.setProperty("Float", new Float(i + 0.1));
            v.setProperty("string", "String");
            v.setProperty("date", new Date(ts));

            v.setProperty("stringArr", new String[] { "foo", "bar" });
            v.setProperty("intArr", new int[] { 1, 2 });
            v.setProperty("longArr", new long[] { 1, 2 });
            v.setProperty("doubleArr", new double[] { 1.1, 2.1 });
            v.setProperty("floatArr", new float[] { 1.1f, 2.1f });
            v.setProperty("booleanArr", new boolean[] { false, true });

            vids[i] = v.getId();

            if (i % numPerCommit == 0) {
                graph.commit();
            }
        }
        graph.commit();

        double duration = System.currentTimeMillis() - ts;
        System.out.println("Took " + duration + "ms to insert " + numVertices + " vertices. Rate = " + (duration / numVertices) + "ms per vertex");

        // Stop and start
        if (isPersistent()) {
            tearDown();
            setUp(false);
        }
        
        // Make sure vertices are still there
        for (int i = 0; i < numVertices; i++) {
            Vertex v = graph.getVertex(vids[i]);

            assertNotNull("Could not find " + vids[i], v);
            //System.out.println("Index " + v.getId() + ". All props: " + v.getPropertyKeys());
            assertEquals(i, v.getProperty("int"));
            assertEquals((long) i, v.getProperty("long"));
            assertEquals(new Integer(i), v.getProperty("Integer"));
            assertEquals(new Long(i), v.getProperty("Long"));
            assertEquals((double) i + 0.1, v.getProperty("double"));
            assertEquals(new Double(i + 0.1), v.getProperty("Double"));
            assertEquals((float) i + 0.1, v.getProperty("float"));
            assertEquals(new Float(i + 0.1), v.getProperty("Float"));
            assertEquals("String", v.getProperty("string"));

            assertEquals("foo", ((String[]) v.getProperty("stringArr"))[0]);
            assertEquals("bar", ((String[]) v.getProperty("stringArr"))[1]);

            assertEquals(1, ((int[]) v.getProperty("intArr"))[0]);
            assertEquals(2, ((int[]) v.getProperty("intArr"))[1]);

            assertEquals(1, ((long[]) v.getProperty("longArr"))[0]);
            assertEquals(2, ((long[]) v.getProperty("longArr"))[1]);

            assertEquals(1.1d, ((double[]) v.getProperty("doubleArr"))[0]);
            assertEquals(2.1d, ((double[]) v.getProperty("doubleArr"))[1]);

            assertEquals(1.1f, ((float[]) v.getProperty("floatArr"))[0]);
            assertEquals(2.1f, ((float[]) v.getProperty("floatArr"))[1]);

            assertEquals(false, ((boolean[]) v.getProperty("booleanArr"))[0]);
            assertEquals(true, ((boolean[]) v.getProperty("booleanArr"))[1]);

            assertEquals(new Date(ts), v.getProperty("date"));
        }
    }

    public void testConcurrency() {
        // Create a vertex
        Vertex v = graph.addVertex(null);
        v.setProperty("foo", "bar");
        
        Edge e = graph.addEdge(null, v, v, "self");
        e.setProperty("foo", "bar");
        Object eid = e.getId();
        
        Object vid = v.getId();
        graph.commit();

        // Create a threaded transaction
        TransactionalGraph graph2 = ((ThreadedTransactionalGraph)graph).newTransaction(); // was startTransaction prior to 2.3.0 port
        
        Vertex v1 = graph.getVertex(vid);
        
        Edge e2 = graph2.getEdge(eid);
        Vertex v2 = e2.getVertex(Direction.OUT);
        
        assertEquals("bar", v1.getProperty("foo"));
        v1.setProperty("foo", "baz");
        
        assertEquals("bar", v2.getProperty("foo"));
        v2.setProperty("foo", "bart");

        // Should succeed
        graph.commit();
        
        try {
            // Should fail
            graph2.commit();
            
            fail("Failed optimistic concurrency test");
        } catch (BitsyRetryException ex) {
            assertEquals(BitsyErrorCodes.CONCURRENT_MODIFICATION, ex.getErrorCode());
        }
        
        Vertex v2retry = graph2.getVertex(vid);
        assertEquals("baz", v2retry.getProperty("foo"));
        
        v2retry.setProperty("foo", "bart");
        graph2.commit();
        
        graph2.shutdown();
        
        // Ensure that the retried transaction came through
        Vertex v3 = graph.getVertex(vid);
        assertEquals("bart", v3.getProperty("foo"));
        
        Edge e3 = v3.getEdges(Direction.OUT).iterator().next();
        assertEquals("self", e3.getLabel());
        assertEquals("bar", e3.getProperty("foo"));

        e3.setProperty("foo", "baz");
        assertEquals("self", e3.getLabel());
        assertEquals("baz", e3.getProperty("foo"));

        TransactionalGraph graph3 = ((ThreadedTransactionalGraph)graph).newTransaction(); // WAS startTransaction prior to 2.3.0 port
        Edge e4 = graph3.getEdge(eid);
        
        assertEquals("self", e4.getLabel());
        assertEquals("bar", e4.getProperty("foo"));

        e4.setProperty("foo", "bart");        
        assertEquals("self", e4.getLabel());
        assertEquals("bart", e4.getProperty("foo"));
        
        assertEquals(e4 + " doesnt' have foo: bart", "bart", e4.getProperty("foo"));
        
        // Should pass
        graph3.commit();

        // See if the changes from graph3 are seen here
        Vertex v51 = graph3.getVertex(vid);
        Edge e51 = v51.getEdges(Direction.OUT).iterator().next();

        assertEquals("self", e51.getLabel());
        assertEquals(e51 + " doesnt' have foo: bart", "bart", e51.getProperty("foo"));
        
        try {
            graph.commit();
            fail("Failed optimistic concurrency test");
        } catch (BitsyRetryException ex) {
            assertEquals(BitsyErrorCodes.CONCURRENT_MODIFICATION, ex.getErrorCode());
        }
        
        // See if the changes from graph3 are seen here
        Vertex v5 = graph.getVertex(vid);
        Edge e5 = v5.getEdges(Direction.OUT).iterator().next();
        
        assertEquals("self", e5.getLabel());
        assertEquals(e5 + " doesnt' have foo: bart", "bart", e5.getProperty("foo"));

        // All old vertices should be dead
        for (Element eN : new Element[] {v1, v2, v2retry, v3, e, e2, e3, e4}) {
            try {
                eN.getProperty("foo");
                fail("dead Tx");
            } catch (BitsyException ex) {
                assertEquals(BitsyErrorCodes.ACCESS_OUTSIDE_TX_SCOPE, ex.getErrorCode());
            }
        }
        
        graph.shutdown();
    }
    
    // Pun intended 
    public void testEdgeCases() {
    	assertEquals(BitsyIsolationLevel.REPEATABLE_READ, ((BitsyGraph)graph).getDefaultIsolationLevel());
    	for (BitsyIsolationLevel level : new BitsyIsolationLevel[] {BitsyIsolationLevel.READ_COMMITTED, BitsyIsolationLevel.REPEATABLE_READ}) {
    		((BitsyGraph)graph).setDefaultIsolationLevel(level);
    		graph.commit();

    		int numVertices = 4;

    		Object[] vids = new Object[numVertices];
    		Vertex[] verts = new Vertex[numVertices];

    		for (int i=0; i < numVertices; i++ ) {
    			Vertex v = graph.addVertex(null);
    			vids[i] = v.getId();
    			verts[i] = v;
    		}

    		// Add an edge from 0 to 1
    		Edge e01 = graph.addEdge(null, verts[0], verts[1], "one");
    		Object e01Id = e01.getId();

    		Edge e01Alt = graph.addEdge(null, verts[0], verts[1], "two");
    		Object e01AltId = e01Alt.getId();

    		Edge e12 = graph.addEdge(null, verts[1], verts[2], "");
    		Object e12Id = e12.getId();

    		Edge e23 = graph.addEdge(null, verts[2], verts[3], "three");
    		Object e23Id = e23.getId();

    		// Check if the edges are in
    		checkIterCount(verts[0].getEdges(Direction.OUT), 2);
    		checkIterCount(verts[0].getEdges(Direction.OUT, "one"), 1);
    		checkIterCount(verts[0].getEdges(Direction.OUT, "two"), 1);

    		checkIterCount(verts[1].getEdges(Direction.OUT), 1);
    		checkIterCount(verts[1].getEdges(Direction.IN), 2);

    		checkIterCount(verts[2].getEdges(Direction.OUT), 1);
    		checkIterCount(verts[2].getEdges(Direction.IN), 1);

    		checkIterCount(verts[3].getEdges(Direction.IN, "three"), 1);
    		checkIterCount(verts[3].getEdges(Direction.IN, "threeX"), 0);

    		// Transaction returns the same object on every get
    		for (int i=0; i < numVertices; i++) {
    			assertSame(verts[i], graph.getVertex(vids[i]));
    		}

    		assertSame(e01, graph.getEdge(e01.getId()));
    		assertSame(e01Alt, graph.getEdge(e01Alt.getId()));
    		assertSame(e12, graph.getEdge(e12.getId()));
    		assertSame(e23, graph.getEdge(e23.getId()));

    		// Now commit
    		graph.commit();
    		
    		// Check to see if the vertices returned for the IDs are the same
    		for (int i=0; i < numVertices; i++) {
    			if (level == BitsyIsolationLevel.REPEATABLE_READ) {
    				Vertex v = graph.getVertex(vids[i]);
    				assertNotNull(v);
    				assertSame(v, graph.getVertex(vids[i]));
    			} else {
    				Vertex v = graph.getVertex(vids[i]);
    				assertNotNull(v);
    				assertNotSame(graph.getVertex(vids[i]), graph.getVertex(vids[i]));
    				assertEquals(graph.getVertex(vids[i]), graph.getVertex(vids[i]));
    				assertEquals(graph.getVertex(vids[i]).hashCode(), graph.getVertex(vids[i]).hashCode());
    			}
    		}

    		// Check to see if mid-tx deletes work
    		for (int i=0; i < numVertices; i++ ) {
    			verts[i] = graph.getVertex(vids[i]);
    		}

    		// Check to see if the end points of edges are the same objects as the loaded vertices
    		e23 = graph.getEdge(e23.getId());
    		if (level == BitsyIsolationLevel.REPEATABLE_READ) {
    			assertSame(verts[2], e23.getVertex(Direction.OUT));
    			assertSame(verts[3], e23.getVertex(Direction.IN));
    		} else {
    			assertEquals(verts[2].getId(), e23.getVertex(Direction.OUT).getId());
    			assertEquals(verts[3].getId(), e23.getVertex(Direction.IN).getId());
    		}
    		
    		// Remove vertex 3
    		graph.removeVertex(verts[2]);

    		// The vertex should not be accessible
    		try {
    			verts[2].getProperty("foo");
    			fail("Can't access deleted vertex");
    		} catch (BitsyException e) {
    			assertEquals(BitsyErrorCodes.ELEMENT_ALREADY_DELETED, e.getErrorCode());
    		}

    		// The edge should not be accessible
    		try {
    			e23.getProperty("foo");
    			fail("Can't access deleted edge");
    		} catch (BitsyException e) {
    			assertEquals(BitsyErrorCodes.ELEMENT_ALREADY_DELETED, e.getErrorCode());
    		}

    		// Check to see that e12 and e23 disappeared from the queries as well
    		checkIterCount(verts[3].getEdges(Direction.IN), 0);
    		checkIterCount(verts[1].getEdges(Direction.OUT), 0);

    		// Now try to load an edge that was not previously in the transaction
    		e12 = graph.getEdge(e12Id);

    		// ... but that edge won't be visible because of the deleted vertex
    		assertNull(e12);

    		// Try the same for e23
    		e23 = graph.getEdge(e23.getId());
    		assertNull(e23);

    		// No commit and recheck
    		graph.commit();

    		// Make sure old elements are deleted
    		assertNull(graph.getVertex(vids[2]));
    		assertNull(graph.getEdge(e23Id));
    		assertNull(graph.getEdge(e12Id));

    		// Get an edge first
    		e01 = graph.getEdge(e01Id);

    		// ... then vertices
    		for (int i=0; i < numVertices; i++ ) {
    			verts[i] = graph.getVertex(vids[i]);
    		}

    		// ... and make sure the endpoints are the same
    		if (level == BitsyIsolationLevel.REPEATABLE_READ) {
    			assertSame(e01.getVertex(Direction.IN), verts[1]);
    			assertSame(e01.getVertex(Direction.OUT), verts[0]);
    		} else {
    			assertEquals(e01.getVertex(Direction.IN).getId(), verts[1].getId());
    			assertEquals(e01.getVertex(Direction.OUT).getId(), verts[0].getId());
    		}
    		
    		// Now remove the edge
    		graph.removeEdge(e01);

    		checkIterCount(verts[0].getEdges(Direction.OUT), 1); // only e01Alt lives
    		checkIterCount(verts[1].getEdges(Direction.IN), 1); // only e01Alt lives

    		// The edge should not be accessible
    		try {
    			e01.getVertex(Direction.IN);
    			fail("Can't access deleted edge");
    		} catch (BitsyException e) {
    			assertEquals(BitsyErrorCodes.ELEMENT_ALREADY_DELETED, e.getErrorCode());
    		}

    		// But the vertex should be
    		e01Alt = graph.getEdge(e01AltId);
    		if (level == BitsyIsolationLevel.REPEATABLE_READ) {
    			assertSame(e01Alt.getVertex(Direction.IN), verts[1]);
        		assertSame(e01Alt.getVertex(Direction.OUT), verts[0]);
    		} else {
    			assertEquals(e01Alt.getVertex(Direction.IN).getId(), verts[1].getId());
    			assertEquals(e01Alt.getVertex(Direction.OUT).getId(), verts[0].getId());
    		}
    	}
    	
    	// Reset default isolation level
    	((BitsyGraph)graph).setDefaultIsolationLevel(BitsyIsolationLevel.REPEATABLE_READ);
    	graph.commit();
    }

    public void testMultiThreadedEdgeQueries() throws IOException {
        // The purpose of this test is to create two vertices with a lot of
        // different types of edges with properties/labels. The edges will be
        // created and removed by multiple threads. A separate read thread will
        // validate that transaction boundaries are respected.
        setException(null);

        Vertex v1 = graph.addVertex(null);
        Vertex v2 = graph.addVertex(null);
        final Object v1Id = v1.getId();
        final Object v2Id = v2.getId();
        graph.commit();

        final int numThreads = 100;
        ExecutorService service = Executors.newFixedThreadPool(numThreads + 2);
        long timeToTest = 10000; // 10 seconds
        final long timeToStop = System.currentTimeMillis() + timeToTest; 

        service.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    int counter = 0;
                    while (System.currentTimeMillis() < timeToStop) {
                        //System.out.println("Read iter");
                        counter++;
                        Vertex v1 = graph.getVertex(v1Id);
                        assertNotNull(v1);
    
                        Vertex v2 = graph.getVertex(v2Id);
                        assertNotNull(v2);
    
                        validate(v1.getEdges(Direction.OUT), v1, v2);
                        validate(v2.getEdges(Direction.IN), v1, v2);
    
                        // This is to reload new vertices in REPEATABLE_READ mode (default)
                        graph.rollback();                    
                        
                        v1 = graph.getVertex(v1Id);
                        assertNotNull(v1);
    
                        v2 = graph.getVertex(v2Id);
                        assertNotNull(v2);
    
                        validate(v1.getEdges(Direction.OUT, "label34", "label17"), v1, v2, "label34", "label17");
                        validate(v2.getEdges(Direction.IN, "label87", "label39"), v1, v2, "label87", "label39");
    
                        // This is to reload new vertices in REPEATABLE_READ mode (default)
                        graph.rollback();

                        //System.out.println("At " + counter + " read iterations");
                    }

                    System.out.println("Completed " + counter + " read iterations");
                } catch (Throwable e) {
                    setException(e);
                }
            }
            
            public void validate(Iterable<Edge> edges, Vertex v1, Vertex v2, String... labels) {
                Map<Integer, Integer> countMap = new HashMap<Integer, Integer>();
                List<String> labelsToCheck = ((labels == null) || (labels.length == 0)) ? null : Arrays.asList(labels);
                for (Edge e : edges) {
                    //System.out.println("Found");
                    assertEquals(v1, e.getVertex(Direction.OUT));
                    assertEquals(v2, e.getVertex(Direction.IN));
                    
                    Integer key = e.getProperty("count");
                    Integer value = countMap.get(key);
                    if (value == null) {
                        countMap.put(key, 1);
                    } else {
                        countMap.put(key, value + 1);
                    }
                    
                    if (labelsToCheck != null) {
                        assertTrue("Could not find " + e.getLabel() + " in " + labelsToCheck, labelsToCheck.contains(e.getLabel()));
                    }
                }
                
                for (Map.Entry<Integer, Integer> entry : countMap.entrySet()) {
                    //System.out.println("Checking count for " + entry.getKey());
                    assertEquals("Transaction boundary not respected for " + entry.getKey() + ", got " + entry.getValue() + " edges", 
                            0, entry.getValue() % entry.getKey());
                }
            }
        });

        final int numEdges = 1;
        for (int i=0; i < numThreads; i++) {
            final String label = "label" + i;
            final int count = i;

            service.submit(new Runnable() {
                @Override
                public void run() {
                    int writeIters = 0;
                    try {
                        Object[] eids = new Object[count * numEdges];
                        int counter = 0;
                        
                        while (System.currentTimeMillis() < timeToStop) {
                            //System.out.println("Write iter");
                            Vertex v1 = graph.getVertex(v1Id);
                            assertNotNull(v1);
    
                            Vertex v2 = graph.getVertex(v2Id);
                            assertNotNull(v2);
    
                            for (int j=0; j < count; j++) {
                                Edge e = v1.addEdge(label, v2);
                                e.setProperty("count", count);
                                eids[counter++] = e.getId();
                            }
    
                            graph.commit();
                            writeIters++;

                            try {
                                Thread.sleep(1 * numThreads);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            if (counter == eids.length) {
                                counter = 0;

                                // Hold at peak -- helps keep the set big
                                try {
                                    Thread.sleep(5 * numThreads);
                                } catch (InterruptedException e1) {
                                    e1.printStackTrace();
                                }
                                
                                // Clear and restart
                                int pauseCounter = 0;
                                for (Object eid : eids) {
                                    graph.getEdge(eid).remove();

                                    if ((pauseCounter > 0) && (pauseCounter++ % count == 0)) {
                                        writeIters++;
                                        graph.commit();

                                        try {
                                            Thread.sleep(5);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        }
                        
                        System.out.println("Completed " + writeIters + " write iterations");
                    } catch (Throwable e) {
                        setException(e);
                    }
                }
            });    
        }
        
        try {
            Thread.sleep(timeToTest + 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (getException() != null) {
            throw new RuntimeException("Got exception", getException());
        }

        System.out.println("Done with multi-threaded edge tests");
        service.shutdown();
    }

    private void setException(Throwable t) {
        this.toThrow = t;
    }
    
    private Throwable getException() {
        return toThrow;
    }

    public void testMultiThreadedTreeCreation() throws IOException {
        // The purpose of this test multithreaded reads and writes. The graph is
        // a sub-graph of a tree whose root node has degree M. Each level
        // reduces the degree to M-1, ..., till 0. There are N threads that
        // start at the root vertex and traverse a random path to the leaf. At
        // each level, the thread checks that the degree is the correct value or
        // 0. If the children have not been created, the thread will create it
        // with some probability.
        //
        // One 'destructor' thread goes around the same tree and zaps nodes with
        // a probability of 1/m! where the degree is supposed to be m. All
        // threads may face CMEs.
        setException(null);

        Vertex rootV = graph.addVertex(null);
        final Object rootVid = rootV.getId(); 
        graph.commit();

        final int rootDegree = 5; // 5! = 120 nodes -- keeping it low to increase chances of BitsyRetryException 
        final int numThreads = 10;
        ExecutorService service = Executors.newFixedThreadPool(numThreads + 1);

        long timeToTest = 20000; // 20 seconds
        final String labelId = "child";
        final long timeToStop = System.currentTimeMillis() + timeToTest; 

        for (int i=0; i < numThreads; i++) {
            service.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        int createIters = 0;
                        while (System.currentTimeMillis() < timeToStop) {
                            Object vid = rootVid;
                            
                            int expectedDegree = rootDegree;
                            while (expectedDegree > 0) {
                                Thread.sleep(rand.nextInt(5)); // Sleep between 0 and 5ms;

                                Vertex v = null;
                                try {
                                    v = graph.getVertex(vid);
                                    int count = 0;
                                    int idx = rand.nextInt(expectedDegree);
                                    for (Edge e : v.getEdges(Direction.OUT, labelId)) {
                                        if (count == idx) {
                                            vid = e.getVertex(Direction.IN).getId();
                                        }
                                        count++;
                                    }
                                    
                                    if (count > 0) {
                                        assertEquals(expectedDegree, count);
                                        expectedDegree--;
                                        continue;
                                    }
                                } catch (BitsyRetryException e) {
                                    // Someone else removed the edges
                                    System.out.println("Got a concurrent mod exception -- expected behavior");
                                    graph.rollback();
                                    
                                    break;
                                }
                                                                
                                // This is the end because count is 0
                                if (expectedDegree == 0) {
                                    // Leaf is OK. Creator is happy. 
                                    assertEquals("no", v.getProperty("haschildren"));
                                    break;
                                } else {                                    
                                    // Probability of 1/3 to create children
                                    if (rand.nextInt() % 3 == 0) {
                                        try {
                                            //System.out.println("Adding nodes with expected degree " + expectedDegree);

                                            // Not at leaf
                                            v.setProperty("haschildren", "yes");
                                            for (int i=0; i < expectedDegree; i++) {
                                                Vertex child = graph.addVertex(null);
                                                child.setProperty("haschildren", "no");
                                                v.addEdge(labelId, child);
                                            }
                                            
                                            createIters++;
                                            graph.commit();
                                        } catch (BitsyRetryException e) {
                                            // Someone else did it
                                            System.out.println("Got a concurrent mod exception -- expected behavior");
                                            graph.rollback();
                                        }
                                    }
                                    
                                    break;
                                }
                            }
                        }
                        
                        System.out.println("Completed " + createIters + " create iterations");
                    } catch (Throwable e) {
                        setException(e);
                    }
                }
            });
        }
        
        // Create the destructor
        service.submit(new Runnable() {
            @Override
            public void run() {
                try {
                    int deleteIters = 0;
                    
                    while (System.currentTimeMillis() < timeToStop) {
                        Object vid = rootVid;
                        
                        int expectedDegree = rootDegree;
                        while (expectedDegree > 0) {
                            Thread.sleep(rand.nextInt(5)); // Sleep between 0 and 5ms;

                            Vertex v = graph.getVertex(vid);
                            int count = 0;
                            int idx = rand.nextInt(expectedDegree);
                            for (Edge e : v.getEdges(Direction.OUT, labelId)) {
                                if (count == idx) {
                                    vid = e.getVertex(Direction.IN).getId();
                                }
                                count++;
                            }

                            if (count == 0) {
                                // Reached the end
                                break;
                            }

                            // There are children under this node -- toss a coin
                            if (rand.nextInt(factorial(expectedDegree)) == 0) {
                                try {
                                    //System.out.println("Removing node with expected degree " + expectedDegree);

                                    // Remove this node and its children
                                    removeDesc(v);

                                    deleteIters++;
                                    graph.commit();
                                } catch (BitsyRetryException e) {
                                    // Someone else did it
                                    System.out.println("Got a concurrent mod exception -- expected behavior");
                                    graph.rollback();
                                }
                                
                                break;
                            }

                            assertEquals(expectedDegree, count);
                            expectedDegree--;
                        }
                    }
                    
                    System.out.println("Completed " + deleteIters + " delete iters");
                } catch (Throwable e) {
                    setException(e);
                }
            }

            private void removeDesc(Vertex v) {
                for (Vertex childV : v.getVertices(Direction.OUT)) {
                    removeDesc(childV);
                    childV.remove();
                }
            }

            private int factorial(int count) {
                int ans = 1;
                for (int i=1; i <= count; i++) {
                    ans *= count;
                }

                return ans;
            }
        });
        
        try {
            Thread.sleep(timeToTest + 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (getException() != null) {
            throw new RuntimeException("Got exception", getException());
        }

        System.out.println("Done with multi-threaded tree tests");
        service.shutdown();
    }

    private void checkIterCount(Iterable<?> iterable, int expectedCount) {
        int count = 0;
        Iterator<?> iter = iterable.iterator();
        while (iter.hasNext()) {
            iter.next();
            count++;
        }
        
        assertEquals(expectedCount, count);
    }
    
    public void testPersistence() throws Exception {
        BitsyGraph bGraph = (BitsyGraph)graph;
        FileBackedMemoryGraphStore store = (FileBackedMemoryGraphStore)(bGraph.getStore());
        
        // Check defaults
        assertEquals(1000, bGraph.getMinLinesPerReorg());
        assertEquals(4 * 1024 * 1024, bGraph.getTxLogThreshold());
        assertEquals(1d, bGraph.getReorgFactor());
        assertTrue(store.allowFullGraphScans());
        
        // Add a little -- shouldn't flush
        int numVertices = 100;
        Object[] vids = new Object[numVertices];
        Vertex[] verts = new Vertex[numVertices];
        
        for (int i=0; i < numVertices; i++ ) {
            Vertex v = graph.addVertex(null);
            v.setProperty("foo", "something \n multi-line");
            vids[i] = v.getId();
            verts[i] = v;
        }
        
        // Commit
        graph.commit();
        
        // Make sure the number of lines are correct -- 100 + 1 header + 1 tx, 1 header in the other
        int txA = lineCount(dbPath.resolve(Paths.get("txA.txt")));
        int txB = lineCount(dbPath.resolve(Paths.get("txB.txt")));
        int vA = lineCount(dbPath.resolve(Paths.get("vA.txt")));
        int vB = lineCount(dbPath.resolve(Paths.get("vB.txt")));
        int eA = lineCount(dbPath.resolve(Paths.get("eA.txt")));
        int eB = lineCount(dbPath.resolve(Paths.get("eB.txt")));
        
        assertPair(102, 1, txA, txB); // 1 header + 100 V + 1 tx, 1 header 
        assertPair(0, 2, vA, eB); // An empty log is flushed in the beginning, hence 1, 2
        assertPair(0, 2, eA, eB);
        
        // Add some edges -- shouldn't flush
        for (int i=0; i < numVertices; i++ ) {
            verts[i] = graph.getVertex(vids[i]);
        }
        
        for (int i=1; i < numVertices; i++ ) {
            graph.addEdge(null, verts[i-1], verts[i], "label\n with multi-lines");
        }

        // Commit
        graph.commit();
        
        // Make sure the number of lines are correct -- 100 + 1 header + 1 tx, 1 header in the other
        txA = lineCount(dbPath.resolve(Paths.get("txA.txt")));
        txB = lineCount(dbPath.resolve(Paths.get("txB.txt")));
        vA = lineCount(dbPath.resolve(Paths.get("vA.txt")));
        vB = lineCount(dbPath.resolve(Paths.get("vB.txt")));
        eA = lineCount(dbPath.resolve(Paths.get("eA.txt")));
        eB = lineCount(dbPath.resolve(Paths.get("eB.txt")));
        
        assertPair(202, 1, txA, txB); // 1 header + 100 V + 1 tx + 99 E + 1tx, 1 header 
        assertPair(0, 2, vA, vB); // An empty log is flushed in the beginning, hence 2
        assertPair(0, 2, eA, eB);
        
        // Trigger a flush
        bGraph.setTxLogThreshold(10); // 10 bytes -- definitely will trigger flush on next add
        
        // Add dummy vertex with no props #101
        graph.addVertex(null);

        graph.commit();
        
        // Wait for flush
        Thread.sleep(1000);
        
        // Make sure the number of lines are correct
        txA = lineCount(dbPath.resolve(Paths.get("txA.txt")));
        txB = lineCount(dbPath.resolve(Paths.get("txB.txt")));
        vA = lineCount(dbPath.resolve(Paths.get("vA.txt")));
        vB = lineCount(dbPath.resolve(Paths.get("vB.txt")));
        eA = lineCount(dbPath.resolve(Paths.get("eA.txt")));
        eB = lineCount(dbPath.resolve(Paths.get("eB.txt")));
        
        assertPair(1, 1, txA, txB); // 1 header, 1 header 
        assertPair(0, 104, vA, vB); // 1 header + 1 log (on load) + 101 V + 1 log (on copy)
        assertPair(0, 102, eA, eB);   // 1 header + 1 log (on load) + 99 E + 1 log (on copy)

        Path stage1 = tempDir("stage1");
        bGraph.backup(stage1);
                
        // Set the min lines to a small value to trigger reorg -- more than 200 lines have been added, so the factor will kick in
        bGraph.setMinLinesPerReorg(1);
        assertEquals(bGraph.getMinLinesPerReorg(), 1);
        
        // Add a dummy vertex #102
        graph.addVertex(null);

        graph.commit();
        
        // Wait for the flush followed by reorg
        Thread.sleep(2000);
        
        // Make sure the number of lines are correct
        txA = lineCount(dbPath.resolve(Paths.get("txA.txt")));
        txB = lineCount(dbPath.resolve(Paths.get("txB.txt")));
        vA = lineCount(dbPath.resolve(Paths.get("vA.txt")));
        vB = lineCount(dbPath.resolve(Paths.get("vB.txt")));
        eA = lineCount(dbPath.resolve(Paths.get("eA.txt")));
        eB = lineCount(dbPath.resolve(Paths.get("eB.txt")));
        
        assertPair(1, 1, txA, txB); // 1 header, 1 header 
        assertPair(0, 104, vA, vB); // 1 header + 102 V + 1 L
        assertPair(0, 101, eA, eB);   // 1 header + 99 E + 1 L
        
        Path stage2 = tempDir("stage2");
        bGraph.backup(stage2);
        
        // Modify the vertices to see if that takes effect
        for (int i=0; i < numVertices; i++ ) {
            // Using the string representation
            verts[i] = graph.getVertex(vids[i].toString());
            verts[i].setProperty("baz", new Date());
        }
        graph.commit();
        
        // Lower reorg factor
        bGraph.setReorgFactor(0.0001d);
        bGraph.setMinLinesPerReorg(0);

        // Wait for the flush followed by reorg
        Thread.sleep(1000);
        
        // Add a dummy vertex #103
        graph.addVertex(null);
        graph.commit();
        
        // Wait for the flush followed by reorg
        Thread.sleep(2000);
        
        // Increase the min lines and the factor to still trigger the reorg
        bGraph.setMinLinesPerReorg(99);
        bGraph.setReorgFactor(0.3d);
        bGraph.setTxLogThreshold(1024 * 1024); // 1MB -- won't trigger flush

        // Make sure the number of lines are correct
        txA = lineCount(dbPath.resolve(Paths.get("txA.txt")));
        txB = lineCount(dbPath.resolve(Paths.get("txB.txt")));
        vA = lineCount(dbPath.resolve(Paths.get("vA.txt")));
        vB = lineCount(dbPath.resolve(Paths.get("vB.txt")));
        eA = lineCount(dbPath.resolve(Paths.get("eA.txt")));
        eB = lineCount(dbPath.resolve(Paths.get("eB.txt")));

        assertPair(1, 1, txA, txB); // 1 header, 1 header 
        assertPair(0, 105, vA, vB); // 1 header + 103 V + 1 L
        assertPair(0, 101, eA, eB);   // 1 header + 99 E + 1 L
        
        Path stage3 = tempDir("stage3");
        bGraph.backup(stage3);
        
        // Remove the vertices
        for (int i=0; i < numVertices; i++ ) {
            verts[i] = graph.getVertex(vids[i].toString());
            
            // This will remove both the vertex and edge
            graph.removeVertex(verts[i]);
        }
        graph.commit();
        
        // Wait for the flush followed by reorg
        Thread.sleep(1000);
        
        // Make sure the number of lines are correct
        txA = lineCount(dbPath.resolve(Paths.get("txA.txt")));
        txB = lineCount(dbPath.resolve(Paths.get("txB.txt")));
        vA = lineCount(dbPath.resolve(Paths.get("vA.txt")));
        vB = lineCount(dbPath.resolve(Paths.get("vB.txt")));
        eA = lineCount(dbPath.resolve(Paths.get("eA.txt")));
        eB = lineCount(dbPath.resolve(Paths.get("eB.txt")));

        assertPair(1, 102, txA, txB); // 1 header, 1 H + 100V header + 1 T 
        //assertPair(0, 105, vA, vB); // 1 header + 103 V + 1 L
        assertPair(0, 106, vA, vB); // 1 header + 103 V + 1 L
        //assertPair(0, 101, eA, eB);   // 1 header + 99 E + 1 L
        assertPair(0, 102, eA, eB);   // 1 header + 99 E + 1 L

        // Backup will flush the buffers explicitly. Reorg will follow after backup
        Path stage4 = tempDir("stage4");
        bGraph.backup(stage4);
        
        // Wait for the flush followed by reorg
        Thread.sleep(2000);
        
        // Make sure the number of lines are correct
        txA = lineCount(dbPath.resolve(Paths.get("txA.txt")));
        txB = lineCount(dbPath.resolve(Paths.get("txB.txt")));
        vA = lineCount(dbPath.resolve(Paths.get("vA.txt")));
        vB = lineCount(dbPath.resolve(Paths.get("vB.txt")));
        eA = lineCount(dbPath.resolve(Paths.get("eA.txt")));
        eB = lineCount(dbPath.resolve(Paths.get("eB.txt")));
        
        assertPair(1, 1, txA, txB); // 1 header, 1 header 
        assertPair(0, 5, vA, vB); // 1 header + 3 dummy V + 1 L
        assertPair(0, 2, eA, eB);   // 1 header + 0 E + 1 L
    }

    private void assertPair(int a, int b, int c, int d) {
        if (a == c) {
            assertEquals(b, d);
        } else {
            assertEquals(a, d);
            assertEquals(b, c);
        }
    }
    
    private int lineCount(Path file) throws Exception {
        InputStream is = new FileInputStream(file.toFile());
        assertNotNull(is);
        BufferedReader br = new BufferedReader(new InputStreamReader(is, FileBackedMemoryGraphStore.utf8));
        
        int ans = 0;
        String line = null;
        while ((line = br.readLine()) != null) {
            ans++;
        }
        
        br.close();
        is.close();
        
        return ans;
    }
    
    public void testLargeFileSaveLoad() throws Exception {
        BitsyGraph bGraph = (BitsyGraph)graph;
        
        if (!(bGraph.getStore() instanceof FileBackedMemoryGraphStore)) {
            return;
        }
        
        FileBackedMemoryGraphStore store = (FileBackedMemoryGraphStore)(bGraph.getStore());
        
        // Check defaults
        assertEquals(1000, bGraph.getMinLinesPerReorg());
        assertEquals(4 * 1024 * 1024, bGraph.getTxLogThreshold());
        assertEquals(1d, bGraph.getReorgFactor());
        assertTrue(store.allowFullGraphScans());
        
        // Add a little -- shouldn't flush
        int numCommit = 1000; // 5000
        int numPerCommit = 100; // 1000
        int numVertices = numCommit * numPerCommit;
        
        //Object[] vids = new Object[numVertices];
        //Vertex[] verts = new Vertex[numVertices];

        long ts = System.currentTimeMillis();
        Vertex prevV = null;
        for (int i=0; i < numVertices; i++ ) {
            Vertex v = graph.addVertex(null);
            v.setProperty("foo", "bar");
            v.setProperty("count", i);
            //vids[i] = v.getId();
            //verts[i] = v;
            
            if (prevV != null) {
                graph.addEdge(null, prevV, v, "test");
            }
            
            if (i % numPerCommit == 0) {
                prevV = null;
                //System.out.println("Commit");
                graph.commit();                
            } else {
                prevV = v;
            }
        }
        
        // Commit
        graph.commit();

        double duration = System.currentTimeMillis() - ts;
        System.out.println("Took " + duration + "ms to insert " + numVertices + " vertices. Rate = " + (duration / numVertices) + "ms per vertex");

        bGraph.flushTxLog();
        
        duration = System.currentTimeMillis() - ts;
        System.out.println("Took " + duration + "ms to insert and flush " + numVertices + " vertices. Rate = " + (duration / numVertices) + "ms per vertex");
        
        // Restart a few times
        for (int j=0; j<3; j++) {
            ts = System.currentTimeMillis();
            tearDown();
            
            duration = System.currentTimeMillis() - ts;
            System.out.println("Took " + duration + "ms to shut down graph with " + numVertices + " vertices. Rate = " + (duration / numVertices) + "ms per vertex");

            ts = System.currentTimeMillis();

            setUp(false);

            duration = System.currentTimeMillis() - ts;
            System.out.println("Took " + duration + "ms to load " + numVertices + " vertices. Rate = " + (duration / numVertices) + "ms per vertex");
            
            ts = System.currentTimeMillis();

            checkIterCount(graph.getVertices(), numVertices);
            duration = System.currentTimeMillis() - ts;
            System.out.println("Took " + duration + "ms to query " + numVertices + " vertices. Rate = " + (duration / numVertices) + "ms per vertex");
        }        
    }

    public void XtestMultiThreadedCommits() throws Exception {
        for (int numThreads : new int[] {1, 2, 3, 4, 5, 10, 25, 50, 100, 150, 250, 500, 750, 1000}) {
            final int numVerticesPerThread = (numThreads <= 10 ? 10000 : (numThreads <= 100 ? 100000 : 100000)) / numThreads;
            int numElements = 2 * numVerticesPerThread * numThreads;
            
            ExecutorService service = Executors.newFixedThreadPool(numThreads);

            final Object[] startVertex = new Object[numThreads];

            final CountDownLatch cdl = new CountDownLatch(numThreads);
            long ts = System.currentTimeMillis();
            final String TEST_LABEL = "test";
            for (int i = 0; i < numThreads; i++) {
                final int tid = i;
                System.out.println("Scheduling write work for thread " + tid);
                service.submit(new Runnable() {
                    @Override
                    public void run() {
                        Object prevId = null;
                        for (int j = 0; j < numVerticesPerThread; j++) {
                            Vertex v = graph.addVertex(null);
                            if (prevId == null) {
                                startVertex[tid] = v.getId();
                            } else {
                                Vertex prevV = graph.getVertex(prevId);
                                graph.addEdge(null, prevV, v, TEST_LABEL);
                            }
                            graph.commit();
                            
                            prevId = v.getId();
                        }
                        
                        System.out.println("Thread " + tid  + " is done");
                        cdl.countDown();
                    }
                });
            }
            
            cdl.await();

            double duration = System.currentTimeMillis() - ts;
            System.out.println("Took " + duration + "ms to save " + numElements + " vertices+edges. Rate = " + (duration / numElements) + "ms per vertex. TPS = " + ((double)numElements * 1000 / duration));
            
            // Wait 10 seconds between tests
            Thread.sleep(10000);
            
            ((BitsyGraph)graph).setDefaultIsolationLevel(BitsyIsolationLevel.READ_COMMITTED);
            final CountDownLatch cdl2 = new CountDownLatch(numThreads);
            ts = System.currentTimeMillis();
            for (int i = 0; i < numThreads; i++) {
                final int tid = i;
                System.out.println("Scheduling read work for thread " + tid);
                service.submit(new Runnable() {
                    @Override
                    public void run() {
                        for (int k=0; k < 100; k++) {
                            int count = 0;
                            Vertex v = graph.getVertex(startVertex[tid]);

                            Edge e;
                            do {
                                Iterator<Edge> eIter = v.getEdges(Direction.OUT).iterator();
                                if (!eIter.hasNext()) {
                                    break;
                                } else {
                                    count++;
                                    v = eIter.next().getVertex(Direction.IN);
                                }
                            } while (true);

                            if (numVerticesPerThread != count + 1) {
                                System.out.println("Mistmatch between " + numVerticesPerThread + " and " + count);
                            }
                            
                            graph.commit();
                        }
                        
                        System.out.println("Thread " + tid  + " is done");
                        cdl2.countDown();
                    }
                });
            }

            cdl2.await();

            duration = System.currentTimeMillis() - ts;
            System.out.println("Took " + duration + "ms to query " + numElements + " vertices+edge 100 times. Rate = " + (duration / numElements) + "ms per vertex. TPS = " + ((double)numElements * 100000 / duration));
            ((BitsyGraph)graph).setDefaultIsolationLevel(BitsyIsolationLevel.REPEATABLE_READ);

            service.shutdown();

            // Uncomment to look at memory usage
//            Thread.sleep(1000);
//            System.gc();
//            Thread.sleep(30000);

            // Clear graph
            tearDown();
            setUp(true);
        }
    }

    public void XtestMultiThreadedReadsOnBipartiteGraph() throws Exception {
        final int numVertices = 1000000; // 100K vertices
        final int numIters = 100000;
        final int numElements = 8 * numIters; // expected to visit 8 v/e per iteration
        final int partSize = numVertices / 2;
        final int numPerCommit = 1000;

        final String label = "test";
        final Object[] outVertices = new Object[partSize];
        final Object[] inVertices = new Object[partSize];
        
        // Vertices
        for (int i=0; i < partSize; i++) {
            outVertices[i] = graph.addVertex(null).getId();
            inVertices[i] = graph.addVertex(null).getId();
            
            if (i % numPerCommit == 0) {
                graph.commit();
            }
        }
        
        // Edges
        for (int i=0; i < partSize; i++) {
            Vertex outVertex = graph.getVertex(outVertices[i]);
            outVertex.addEdge(label, graph.getVertex(inVertices[(5 * i + 1) % partSize]));
            outVertex.addEdge(label, graph.getVertex(inVertices[(5 * i + 4) % partSize]));
            outVertex.addEdge(label, graph.getVertex(inVertices[(5 * i + 7) % partSize]));

            if (i % numPerCommit == 0) {
                graph.commit();
            }
        }
        graph.commit();

        final int numRuns = 3;
        Map<Integer, String> calcStrMap = new HashMap<Integer, String>();
        
        for (int run=0; run < numRuns; run++) {
            ((BitsyGraph)graph).setDefaultIsolationLevel(BitsyIsolationLevel.READ_COMMITTED);
            for (final int numThreads : new int[] {1, 2, 3, 4, 5, 10, 25, 50, 100, 150, 250, 500, 750, 1000}) {
                ExecutorService service = Executors.newFixedThreadPool(numThreads);
    
                final CountDownLatch cdl = new CountDownLatch(numThreads);
                long ts = System.currentTimeMillis();
    
                ((BitsyGraph)graph).setDefaultIsolationLevel(BitsyIsolationLevel.READ_COMMITTED);
                System.out.println("Running bi-partite read test with " + numThreads + " threads");
                for (int i = 0; i < numThreads; i++) {
                    final int tid = i;
                    //System.out.println("Scheduling read work for thread " + tid);
                    service.submit(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Vertex v = graph.getVertex(outVertices[0]);
                                for (int k=0; k < 100 * numIters / numThreads; k++) {
                                    assertNotNull(v);
    
                                    Vertex nextV = randomVertex(v.getVertices(Direction.OUT, label));
                                    
                                    assertNotNull(nextV);
        
                                    // Take a random edge back
                                    Vertex backV = randomVertex(nextV.getVertices(Direction.IN));                                    
                                    if (backV != null) {
                                        v = backV;
                                    }
                                }
                                
                                //System.out.println("Thread " + tid  + " is done");
                            } catch (Throwable t) {
                                setException(t);
                            } finally {
                                cdl.countDown();
                            }
                        }

                        private Vertex randomVertex(Iterable<Vertex> vertices) {
                            List<Vertex> options = new ArrayList<Vertex>();
                            for (Vertex option : vertices) {
                                options.add(option);
                            }
                            
                            if (options.isEmpty()) {
                                return null;
                            } else {
                                return options.get(rand.nextInt(options.size()));
                            }
                        }
                    });
                }
    
                cdl.await();
                
                if (getException() != null) {
                    throw new RuntimeException("Error in testMultiThreadedReadsOnBipartiteGraph", getException());
                }
                
                service.shutdown();
    
                long duration = System.currentTimeMillis() - ts;
                double tps = ((double)numElements * 100000 / duration);
                System.out.println("Took " + duration + "ms to query " + numElements + " vertices+edge 100 times. Rate = " + (duration / numElements) + "ms per vertex. TPS = " + tps);

                String calcStr = calcStrMap.get(numThreads);
                if (calcStr == null) {
                    calcStrMap.put(numThreads, "=(" + tps);
                } else {
                    calcStrMap.put(numThreads, calcStr + " + " + tps);
                }
            }
        }

        for (Map.Entry<Integer, String> entry : calcStrMap.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue() + ")/3");
        }
        
        ((BitsyGraph)graph).setDefaultIsolationLevel(BitsyIsolationLevel.REPEATABLE_READ);
    }

    public void testConcurrentAccessForIndexes() {
        // Create an index
        ((KeyIndexableGraph)graph).dropKeyIndex("testKey", Vertex.class);
        ((KeyIndexableGraph)graph).createKeyIndex("testKey", Vertex.class);
        
    	for (BitsyIsolationLevel level : new BitsyIsolationLevel[] {BitsyIsolationLevel.REPEATABLE_READ, BitsyIsolationLevel.READ_COMMITTED}) {
    		((BitsyGraph)graph).setDefaultIsolationLevel(level);
    		graph.commit();
    		
	        Vertex v = graph.addVertex(null);
	        v.setProperty("testKey", "foo");
	        
	        graph.commit();
	        
	        Vertex v1 = graph.addVertex(null);
	        v1.setProperty("testKey", "foo");
	        
	        // Don't commit yet
	        TransactionalGraph anotherGraph = ((ThreadedTransactionalGraph)graph).newTransaction();
	        Iterable<Vertex> indexResult = anotherGraph.getVertices("testKey", "foo");
	        
	        checkIterCount(indexResult, 1); // Queried before the commit
	        
	        // Now commit the new vertex
	        graph.commit();
	        
	        checkIterCount(indexResult, 2); // Queried after the commit -- the iterator refreshes itself
	        anotherGraph.commit();
	        
	        anotherGraph = ((ThreadedTransactionalGraph)graph).newTransaction();
	        indexResult = anotherGraph.getVertices("testKey", "foo");
	        checkIterCount(indexResult, 2); // Queried after the commit
	        anotherGraph.commit();
	        
	        graph.getVertex(v.getId()).remove();
	        graph.getVertex(v1.getId()).remove();
	        graph.commit();
    	}
        
    	((BitsyGraph)graph).setDefaultIsolationLevel(BitsyIsolationLevel.REPEATABLE_READ);
        // Drop the index
        ((KeyIndexableGraph)graph).dropKeyIndex("testKey", Vertex.class);
    }
    
    public void testConcurrentAccessForIndexes2() {
        // Create an index
        ((KeyIndexableGraph)graph).dropKeyIndex("testKey", Vertex.class);
        ((KeyIndexableGraph)graph).createKeyIndex("testKey", Vertex.class);
        
    	for (BitsyIsolationLevel level : new BitsyIsolationLevel[] {BitsyIsolationLevel.READ_COMMITTED, BitsyIsolationLevel.REPEATABLE_READ}) {
    	    System.out.println("Testing for " + level);
    		Vertex v = graph.addVertex(null);
	        v.setProperty("testKey", "foo");
	        
	        graph.commit();
	        
	        // Now read the vertex
	        ((BitsyGraph)graph).setTxIsolationLevel(level);
    		Vertex vBak = graph.getVertex(v.getId());
	
	        // ... and update it in a different thread
	        TransactionalGraph anotherGraph = ((ThreadedTransactionalGraph)graph).newTransaction();
	        Vertex v2 = anotherGraph.getVertices("testKey", "foo").iterator().next();
	        v2.setProperty("testKey", "bar");
	        anotherGraph.commit();
	        Vertex vBak2 = anotherGraph.getVertex(v2.getId());
	        
	        // Re-query the old version with the new value
	        if (level == BitsyIsolationLevel.READ_COMMITTED) {
	        	Vertex vQuery = graph.getVertices("testKey", "bar").iterator().next();
		        vQuery.setProperty("testKey", "baz");
	        	// Now commit the new vertex -- should succeed
	        	graph.commit();
	        } else {
	        	assertFalse(graph.getVertices("testKey", "bar").iterator().hasNext());
	        }
    	}
    	
        // Drop the index
        ((KeyIndexableGraph)graph).dropKeyIndex("testKey", Vertex.class);
    }
    
    public void testConcurrentAccessForVersions() throws InterruptedException {
        Vertex v = graph.addVertex(null);
        final Object vid = v.getId();        
        v.setProperty("testKey", "foo");
        graph.commit();
        
        v = graph.getVertex(vid);
        assertEquals("foo", v.getProperty("testKey"));

        Thread t = new Thread() {
            public void run() {
                Vertex v = graph.getVertex(vid);
                v.setProperty("testKey", "bar");
                graph.commit();
            }
        };
        
        t.start();
        t.join();
        
        assertEquals("foo", v.getProperty("testKey"));

        try {
            v.setProperty("testKey", "baz");
            graph.commit();
            
            fail("Should throw concurrent mod exception");
        } catch (BitsyRetryException e) {
            // Ignore
        }
        
        v = graph.getVertex(vid);
        assertEquals("bar", v.getProperty("testKey"));
        v.setProperty("testKey", "baz");
        assertEquals("baz", v.getProperty("testKey"));
        graph.commit();
        
        v = graph.getVertex(vid);
        Vertex v2 = graph.addVertex(null);
        
        Edge e12 = v.addEdge("foo", v2);
        
        v.remove();
        graph.removeVertex(v2);
        
        try {
            v.remove();
            fail("Should throw exception");
        } catch (BitsyException e) {
            assertEquals(BitsyErrorCodes.ELEMENT_ALREADY_DELETED, e.getErrorCode());
        }
        
        try {
            e12.remove();
        } catch (BitsyException e) {
            assertEquals(BitsyErrorCodes.ELEMENT_ALREADY_DELETED, e.getErrorCode());
        } 
        
        try {
            v2.remove();
            fail("Should throw exception");
        } catch (BitsyException e) {
            assertEquals(BitsyErrorCodes.ELEMENT_ALREADY_DELETED, e.getErrorCode());
        }
        
        graph.commit();
    }
    
    public void testLargeDegreePerformance() {
        long ts = System.currentTimeMillis();

        Vertex one = graph.addVertex(null);
        one.setProperty("one", "1");

        int numVertices = 10000; // 1000000;
        Object[] vids = new Object[numVertices];
        for (int i = 0; i < numVertices; i++) { // Change to 1M for perf
            Vertex many = graph.addVertex(null);
            many.setProperty("many", "2");
            graph.addEdge(null, one, many, "toMany");
            vids[i] = many.getId();

            if (i % 1000 == 0) {
                System.out.println(i + " 1000 in " + (System.currentTimeMillis() - ts));
                ts = System.currentTimeMillis();

                graph.commit();
            }
        }
        
        for (int i=0; i < numVertices; i++) {
            Vertex v = graph.getVertex(vids[i]);

            Iterator<Edge> iter = v.getEdges(Direction.BOTH).iterator();
            assertTrue(iter.hasNext());
            iter.next();
            assertFalse(iter.hasNext());

            v.remove();

            if (i % 1000 == 0) {
                System.out.println(i + " 1000 in " + (System.currentTimeMillis() - ts));
                
                if (i % 5000 == 0) {
                    Iterator<Edge> iter2 = graph.getVertex(one.getId()).getEdges(Direction.BOTH).iterator();
                    for (int j=0; j < numVertices - i - 1; j++) {
                        assertTrue(iter2.hasNext());
                        iter2.next();
                    }
                    assertFalse(iter.hasNext());
                }
                
                ts = System.currentTimeMillis();

                graph.commit();
            }
        }

        graph.commit();
    }
}
